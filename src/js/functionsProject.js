//Fechar nav ao clicar
$('.navbar-nav>li>a').on('click', function(){
    $('.navbar-collapse').collapse('hide');
});

//Scroll Page
$('a.scroll_section').on('click',function (e) {
    e.preventDefault();
    var target = this.hash,
    $target = $(target);

   $('html, body').stop().animate({
     'scrollTop': $target.offset().top -10
    }, 2500, 'swing', function () {
     window.location.hash = target;
    });
});

//slick menu
$(window).on('scroll',function() {
  var scrolltop = $(this).scrollTop();

  if(scrolltop >= 40) {
    $('.navClient').addClass('fixClient');
  }

  else if(scrolltop <= 40) {
    $('.navClient').removeClass('fixClient');
  }

  if(scrolltop >= 1200) {
      $(".animated").addClass("fadeInUp");
  } 

});

$(document).ready(function() {
  $('#cadastroHeader').validate({
    rules: {
        nome: "required",
        email: "required",
        cidade: "required",
        uf: "required",
        curso: "required",
        telefone: "required"        
      },
      messages: {
        nome: "Insira seu nome",
        email: "Insira um e-mail",
        cidade: "Insira sua cidade",
        uf: "Selecione seu estado",
        curso: "Selecione um curso",
        telefone: "Insira seu telefone"
      },
      submitHandler: function() {
        //alert("submitted!");
        location.href="../sucesso-header.html"
      }
    });

  $('#cadastroBody').validate({
    rules: {
        nome2: "required",
        email2: "required",
        cidade2: "required",
        uf2: "required",
        curso2: "required",
        telefone2: "required"        
      },
      messages: {
        nome2: "Insira seu nome",
        email2: "Insira um e-mail",
        cidade2: "Insira sua cidade",
        uf2: "Selecione seu estado",
        curso2: "Selecione um curso",
        telefone2: "Insira seu telefone"
      },
      submitHandler: function() {
        //alert("submitted!");
        location.href="../sucesso-body.html"
      }
    });
    $('.cursos_fps li').click(function(e) {
      // e.preventDefault();
      // $(this).toggleClass("active");
      if ( $(this).hasClass('active') ) {
           $(this).removeClass('active');
      } else {
           $('.cursos_fps li').removeClass('active');
           $(this).addClass('active');    
      }
    });
});

//SVG Imagem
jQuery('img.svg').each(function(){
  var $img = jQuery(this);
  var imgID = $img.attr('id');
  var imgClass = $img.attr('class');
  var imgURL = $img.attr('src');

  jQuery.get(imgURL, function(data) {
      // Get the SVG tag, ignore the rest
      var $svg = jQuery(data).find('svg');

      // Add replaced image's ID to the new SVG
      if(typeof imgID !== 'undefined') {
          $svg = $svg.attr('id', imgID);
      }
      // Add replaced image's classes to the new SVG
      if(typeof imgClass !== 'undefined') {
          $svg = $svg.attr('class', imgClass+' replaced-svg');
      }

      // Remove any invalid XML tags as per http://validator.w3.org
      $svg = $svg.removeAttr('xmlns:a');

      // Replace image with new SVG
      $img.replaceWith($svg);

  }, 'xml');

});