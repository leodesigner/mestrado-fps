<?php

$errorMSG = "";

// NAME
if (empty($_POST["name"])) {
    $errorMSG = "Insira um nome ";
} else {
    $name = $_POST["name"];
}

// EMAIL
if (empty($_POST["email"])) {
    $errorMSG .= "Insira um E-mail";
} else {
    $email = $_POST["email"];
}

// TELEFONNE
if (empty($_POST["telefone"])) {
    $errorMSG .= "Insira um Telefone";
} else {
    $telefone = $_POST["telefone"];
}

// MSG SUBJECT
if (empty($_POST["empresa"])) {
    $errorMSG .= "Insira o nome da empresa";
} else {
    $empresa = $_POST["empresa"];
}


// MESSAGE
if (empty($_POST["message"])) {
    $errorMSG .= "A caixa de mensagem é origatória";
} else {
    $message = $_POST["message"];
}


$EmailTo = "contato@proauditauditoria.com.br";
$Subject = "Formulário de Contato do Site";

// prepare email body text
$Body = "";
$Body .= "Name: ";
$Body .= $name;
$Body .= "\n";
$Body .= "Telefone: ";
$Body .= $telefone;
$Body .= "\n";
$Body .= "Email: ";
$Body .= $email;
$Body .= "\n";
$Body .= "Empresa: ";
$Body .= $empresa;
$Body .= "\n";
$Body .= "Messagem: ";
$Body .= $message;
$Body .= "\n";

// send email
$success = mail($EmailTo, $Subject, $Body, "From:".$email);

// redirect to success page
if ($success && $errorMSG == ""){
   echo "success";
}else{
    if($errorMSG == ""){
        echo "Something went wrong :(";
    } else {
        echo $errorMSG;
    }
}

?>